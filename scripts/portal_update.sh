#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

if [ ! $# -eq 1 ]
  then
    echo "** Error: called with $# args. usage: 'portal_update.sh ALL|SOFTWARE|PAGES|CUSTOMIZATIONS'"
    echo "   SOFTWARE: replace $TOMCATWEBAPPS/txmweb with the war directory content"
    echo "   PAGES: replace $TOMCATWEBAPPS/html&css&images with the 'txm' war directory content"
    echo "   CUSTOMIZATIONS: replace $TOMCATWEBAPPS/txmweb with the 'myportal' directory content"
    exit 1
fi

OPTION=$1

echo "Un-deploying current portal version"
if [ ! -d "${TOMCATWEBAPPS}" ]; then
	echo "** portal is not deployed. Deploy it before."
	exit 1
fi

echo "Deploying a new portal version"

#sudo cp -rf "$INSTALLFILES/txm" $TOMCATWEBAPPS
if [ "ALL" == $OPTION ] || [ "PAGES" == $OPTION ] ; then
	echo " updating pages..."
	sudo rm -rf $TOMCATWEBAPPS/txm/html.back
	sudo rm -rf $TOMCATWEBAPPS/txm/css.back
	sudo rm -rf $TOMCATWEBAPPS/txm/images.back

	if [ -d "$TOMCATWEBAPPS/txm/html" ]; then
		sudo mv -f $TOMCATWEBAPPS/txm/html $TOMCATWEBAPPS/txm/html.back
	fi
	if [ -d "$TOMCATWEBAPPS/txm/css" ]; then
		sudo mv -f $TOMCATWEBAPPS/txm/css $TOMCATWEBAPPS/txm/css.back
	fi
	if [ -d "$TOMCATWEBAPPS/txm/images" ]; then
		sudo mv -f $TOMCATWEBAPPS/txm/images $TOMCATWEBAPPS/txm/images.back
	fi

	sudo cp -rf "$INSTALLFILES/txm/html" $TOMCATWEBAPPS/txm
	sudo cp -rf "$INSTALLFILES/txm/css" $TOMCATWEBAPPS/txm
	sudo cp -rf "$INSTALLFILES/txm/images" $TOMCATWEBAPPS/txm
fi


if [ "ALL" == $OPTION ] || [ "SOFTWARE" == $OPTION ] ; then
	echo " updating software files..."
	sudo service tomcat9 stop

	sudo rm -rf $TOMCATWEBAPPS/txm/txmweb.back
	if [ -d "$TOMCATWEBAPPS/txm/txmweb" ]; then
		sudo mv -f $TOMCATWEBAPPS/txm/txmweb $TOMCATWEBAPPS/txm/txmweb.back
	fi
	sudo cp -rf "$INSTALLFILES/txm/txmweb" $TOMCATWEBAPPS/txm

	sudo rm -rf $TOMCATWEBAPPS/txm/WEB-INF.back
	if [ -d "$TOMCATWEBAPPS/txm/WEB-INF" ]; then
		sudo mv -f $TOMCATWEBAPPS/txm/WEB-INF $TOMCATWEBAPPS/txm/WEB-INF.back
	fi
	sudo cp -rf "$INSTALLFILES/txm/WEB-INF" $TOMCATWEBAPPS/txm

	sudo service tomcat9 start
fi


#if [ "ALL" == $OPTION ] || [ "CUSTOMIZATIONS" == $OPTION ] ; then
#	echo " updating personal files..."
#	./portal_update_files.sh
#fi

sudo chown -R tomcat:tomcat $PORTALWEBAPP

