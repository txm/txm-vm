#!/bin/bash

#FUNCTIONS
search_replace_check () {
	THEFILE=$1
	SEARCH=$2
	REPLACE=$3
	if [ ! -e "${THEFILE}" ]; then
		echo "** ERROR: ${THEFILE} does not exists."
		return 1
	fi

	find "$THEFILE" -type f -exec sed -i "s%$SEARCH%$REPLACE%g" {} \;

	if [ $? != 0 ]; then
		echo "** ERROR: failed to find&sed ${THEFILE}"
		return 1;
	fi

	grep "$REPLACE" "$THEFILE" > /dev/null

	if [ $? != 0 ]; then
		echo "** ERROR: failed to find '$REPLACE' in resulting '${THEFILE}'"
		return 1;
	fi

	return 0;
}

#FUNCTIONS

is_backed_file () {
	if [ -e "$1.back" ]; then
		return 0
	fi
	return 1
}

restore_file () {
	if is_backed_file "$1" ; then
		echo "** Restoring $1..."
		sudo cp -f "$1.back" "$1" && 
		sudo rm -f "$1.back"

		if [ $? != 0 ]; then
			echo "** Error: failed to restore $1"
			return 1;
		fi
	else 
		echo "** No backup file to restore for $1."
		return 1;
	fi

	return 0
}

backup_file () {
	if is_backed_file "$1" ; then

		echo "** Error: cannot backup file $1. A backup file already exists."
		return 1;
	else 
		echo "** Backing up $1 to $1.back..."
		sudo cp -f "$1" "$1.back"

		if [ $? != 0 ]; then
			echo "** Error: failed to restore $1"
			return 1;
		fi
	fi

	return 0
}

export -f search_replace_check
export -f restore_file
export -f backup_file
export -f is_backed_file
