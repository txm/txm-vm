#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

if [ ! -d "$PORTALWEBAPP" ]; then
	echo "** Error the portal webapp is not loaded: $PORTALWEBAPP"
	exit 1;
fi

echo "Updating yourportal configuration files..."
sudo cp -rf $INSTALLFILES/yourportal/private/* "$PORTALCONF"
sudo chown -R tomcat:tomcat "$PORTALCONF"

echo "Updating yourportal HTML files..."
sudo cp -rf $INSTALLFILES/yourportal/public/* $PORTALWEBAPP
sudo chown -R tomcat:tomcat $PORTALWEBAPP
