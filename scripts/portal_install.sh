#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

if [ -d "$PORTALWEBAPP" ]; then
	echo "A portal is already running: $PORTALWEBAPP"
	exit 1;
fi

if [ -d "$PORTALCONF" ]; then
	echo "A portal is already installed: $PORTALCONF"
	exit 1;
fi

echo "Install 'last' portal default configuration to $PORTALCONF"

# copy portal configuration files
sudo rm -rf "$PORTALCONF"
sudo mkdir -p "$TOMCATHOME/"
sudo cp -rf "$INSTALLFILES/TXMWEB" "$TOMCATHOME/"

sudo chown -R tomcat:tomcat "$TOMCATHOME/TXMWEB"


# copy portal configuration files
echo "Install 'last' portal war to $PORTALWEBAPP"

sudo rm -rf "$PORTALWEBAPP/txm"
sudo cp -rf "$INSTALLFILES/txm" "$PORTALWEBAPP"

# copy the portal software

./$SCRIPT_PATH/portal_update.sh ALL
