#!/bin/bash
# this script purpose is to mimic the txm.human-num.fr VM clone process to this test VM.

# only TXM related files are copyied from the txm.huma-num.fr VM


read -n 1 -p "This script will update the content of /home/txmadmin, /TXMWEB/txm and /var/lib/tomcat9/webapps/txm using files of the TXM VM. Press y/N to continue or stop.: " mainmenuinput
if [ "$mainmenuinput" = "y" ]; then
	echo " Fetching files...."
else
	echo "Aborted."
	exit 1
fi


rsync -avz -e ssh /home/txmadmin/portal_software_releases txmadmin@txm.huma-num.fr:/home/txmadmin/portal_software_releases &&
rsync -avz -e ssh /TXMWEB/txm txmadmin@txm.huma-num.fr:/TXMWEB/txm &&
rsync -avz -e ssh /var/lib/tomcat9/webapps/txm txmadmin@txm.huma-num.fr:/var/lib/tomcat9/webapps/txm &&

if [ $? != 0 ]; then
	echo "** Error: see logs above."
	exit 1;
fi

exit 0


