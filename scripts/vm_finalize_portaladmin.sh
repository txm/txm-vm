#!/bin/bash

echo "** Finalize portaladmin script: start"

# TODO after postclonage step

MACHINE="$1"
ADMIN_MAIL="$2"
CONTACT_MAIL="$3"

PASSWORD=`apg -m 16 -M SNCL -c /dev/random|head -1`
MAIL_CONTENT="
Votre nouveau portail TXM est disponible, voici ses coordonnées :

    • adresse d’accès au portail : https://${MACHINE}.huma-num.fr
    • adresse du serveur hébergeant le portail : ${MACHINE}.huma-num.fr
    • compte pour la connexion au serveur (SSH, SCP, SFTP) :
        ◦ identifiant : portaladmin
        ◦ mot de passe : $PASSWORD
    • adresse mail privée de l'administrateur du portail* : ${EMAIL_ADMIN}
    • adresse mail publique de contact pour l'administration du portail** : ${EMAIL_CONTACT_PUBLIC}

Merci de réaliser rapidement les opérations indiquées dans la section 2.2.1

Changer les serrures du document « Manuel d'administration du logiciel portail web TXM 0.6.3 - prise en main rapide  » 
téléchargeable à l’adresse : https://frama.link/txm-portal-admin-quick-start

La page d’accueil de votre portail vous guidera pour la suite des opérations : 
	https://${MACHINE}.huma-num.fr/txm

Nous vous invitons par ailleurs à vous inscrire à la liste de discussion dédiée aux administrateurs de portails TXM :
	https://groupes.renater.fr/sympa/info/txm-admin
qui est le réseau d'entraide des administrateurs de portails TXM.

________________________________________

Notes :

* pour les échanges sur la maintenance du portail
** pour les échanges avec les utilisateurs du portail
"
if [ $? != 0 ]; then
        echo "** Error: failed to generate portaladmin password"
        exit 1;
fi

echo "portaladmin:$PASSWORD" | sudo chpasswd

if [ $? != 0 ]; then
	echo "** Error: failed to update portaladmin password"
	exit 1;
fi

echo "**** MAIL TEMPLATE - start ****"
echo "----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< -----"
echo -e "$MAIL_CONTENT"
echo "----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< ----- 8< -----"
echo "**** MAIL TEMPLATE - end ****"

echo "** Finalize portaladmin script: done"
