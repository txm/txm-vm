#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

## stop tomcat because TXM portal might write some files
./$SCRIPT_PATH/portal_stop.sh

echo "** remove $PORTALCONF configuration files"
sudo rm -rf "$PORTALCONF"

echo "** remove txm WEBAPP files"
./$SCRIPT_PATH/portal_undeploy.sh
