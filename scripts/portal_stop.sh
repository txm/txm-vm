#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

if [ ! -d "$PORTALWEBAPP" ]; then
	echo "** Error the portal webapp is not deployed: $PORTALWEBAPP"
	exit 1;
fi

sudo service tomcat9 stop
