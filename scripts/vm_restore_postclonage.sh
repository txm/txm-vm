#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

echo "** Restoring TXM backup files: "

#SCRIPT

restore_file "$PORTALCONF_TBX"

restore_file "$PORTALCONF_WEB"

restore_file "$PORTALWEBAPP_ENTRYPOINT"

restore_file "$PORTALWEBAPP_HOME"

restore_file "$PORTALWEBAPP_CONTACT"

restore_file "$PORTALCONF_ADMIN_USER"

restore_file "$APACHE_DEFAULT_CONF"

restore_file "$APACHE_DEFAULT_LE_CONF"

restore_file "$APACHE_DEFAULT_LE_CONF2"

echo "** Restoring TXM backup files: done"
