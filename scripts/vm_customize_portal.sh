#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

echo "** Customizing TXM portal"

if [ ! $# -eq 3 ]
  then
    echo "** Error: call with $# args. usage: 'vm_customize_portal.sh MACHINE-NAME PORTAL-ADMIN-MAIL PORTAL-CONTACT-MAIL'"
    exit 1
fi

#PARAMETERS

MACHINE="$1"
PROJECT="$1"
ADMIN_MAIL="$2"
CONTACT_MAIL="$3"

#SCRIPT

if is_backed_file "${PORTALCONF_TBX}"; then
	echo "** ${PORTALCONF_TBX} has already been modified. Remove ${PORTALCONF_TBX}.back or call vm_restore_postclonage.sh"
	exit 1
fi

if is_backed_file "${PORTALCONF_WEB}"; then
	echo "** ${PORTALCONF_WEB} has already been modified. Remove ${PORTALCONF_WEB}.back or call vm_restore_postclonage.sh"
	exit 1
fi
if is_backed_file "${PORTALWEBAPP_ENTRYPOINT}" ; then
	echo "** ${PORTALWEBAPP_ENTRYPOINT} has already been modified. Remove ${PORTALWEBAPP_ENTRYPOINT}.back or call vm_restore_postclonage.sh"
	exit 1
fi
if is_backed_file "${PORTALWEBAPP_HOME}" ; then
	echo "** ${PORTALWEBAPP_HOME} has already been modified. Remove ${PORTALWEBAPP_HOME}.back or call vm_restore_postclonage.sh"
	exit 1
fi
if is_backed_file "${PORTALWEBAPP_CONTACT}" ; then
	echo "** ${PORTALWEBAPP_CONTACT} has already been modified. Remove ${PORTALWEBAPP_CONTACT}.back or call vm_restore_postclonage.sh"
	exit 1
fi
if is_backed_file "${PORTALCONF_ADMIN_USER}" ; then
	echo "** ${PORTALCONF_ADMIN_USER} has already been modified. Remove ${PORTALCONF_ADMIN_USER}.back or call vm_restore_postclonage.sh"
	exit 1
fi
if is_backed_file "${ROOTPORTALWEBAPP_ENTRYPOINT}" ; then
	echo "** ${ROOTPORTALWEBAPP_ENTRYPOINT} has already been created. Remove ${ROOTPORTALWEBAPP_ENTRYPOINT} or call vm_restore_postclonage.sh"
	exit 1
fi

echo "** Stopping tomcat."

service tomcat9 stop

echo "** updating $PORTALCONF_TBX"

backup_file "$PORTALCONF_TBX" && 

search_replace_check "$PORTALCONF_TBX" "=/TXMWEB/txm" "=${TOMCATHOME}/TXMWEB/txm" && 

chown tomcat:tomcat "$PORTALCONF_TBX"


echo "** updating $PORTALCONF_WEB"

backup_file "$PORTALCONF_WEB" && 

search_replace_check "$PORTALCONF_WEB" "=/TXMWEB/txm" "=${TOMCATHOME}/TXMWEB/txm" && 

search_replace_check "$PORTALCONF_WEB" "mail.default.from=.*" "mail.default.from=noreply@${MACHINE}.huma-num.fr" && 

search_replace_check "$PORTALCONF_WEB" "mail.default.reply=.*" "mail.default.reply=${CONTACT_MAIL}" && 

search_replace_check "$PORTALCONF_WEB" "portal_address=.*" "portal_address=https://${MACHINE}.huma-num.fr/txm/" && 

search_replace_check "$PORTALCONF_WEB" "portal_name=.*" "portal_name=${PROJECT}" && 

search_replace_check "$PORTALCONF_WEB" "portal_longname=.*" "portal_longname=${PROJECT}" && 

search_replace_check "$PORTALCONF_WEB" "nomail=.*" "nomail=false" && 

chown tomcat:tomcat "$PORTALCONF_WEB"

if [ $? != 0 ]; then
	echo "** Error: failed to update ${PORTALCONF_WEB}"
	exit 1;
fi

#delete the 'admin' user file, so it will be re-generated
echo "** updating $PORTALCONF_ADMIN_USER"

backup_file "$PORTALCONF_ADMIN_USER" &&

search_replace_check "$PORTALCONF_ADMIN_USER" "institution=\".*\"" "institution=\"$MACHINE\"" &&
search_replace_check "$PORTALCONF_ADMIN_USER" "mail=\".*\"" "mail=\"$ADMIN_MAIL\"" &&

if [ $? != 0 ]; then
        echo "** Error: failed to update ${PORTALCONF_ADMIN_USER}"
        exit 1;
fi

echo "** updating $PORTALWEBAPP_ENTRYPOINT"

backup_file "$PORTALWEBAPP_ENTRYPOINT" &&

#titles
search_replace_check "$PORTALWEBAPP_ENTRYPOINT" "<title>.*</title>" "<title>${PROJECT}</title>" &&
search_replace_check "$PORTALWEBAPP_ENTRYPOINT" "<h4 id=\"bienvenue\">.*</h4>" "<h4 id=\"bienvenue\">Bienvenue sur le portail $PROJECT</h4>" &&

chown tomcat:tomcat "$PORTALWEBAPP_ENTRYPOINT"

if [ $? != 0 ]; then
	echo "** Error: failed to update ${PORTALWEBAPP_ENTRYPOINT}"
	exit 1;
fi


echo "** updating $PORTALWEBAPP_HOME"

backup_file "$PORTALWEBAPP_HOME" &&

search_replace_check "$PORTALWEBAPP_HOME" "<h1>Bienvenue au portail MACHINE</h1>" "<h1>Bienvenue au portail ${MACHINE}</h1>" &&

chown tomcat:tomcat "$PORTALWEBAPP_HOME"

if [ $? != 0 ]; then
	echo "** Error: failed to update ${PORTALWEBAPP_HOME}"
	exit 1;
fi


echo "** updating $PORTALWEBAPP_CONTACT"

TMP_EMAIL="${CONTACT_MAIL/@/ AT }"
TMP_EMAIL="${TMP_EMAIL/./ DOT }"

backup_file "$PORTALWEBAPP_CONTACT" &&

search_replace_check "$PORTALWEBAPP_CONTACT" ">textometrie AT ens-lyon DOT fr<" ">${TMP_EMAIL}<" &&

chown tomcat:tomcat "$PORTALWEBAPP_CONTACT"

if [ $? != 0 ]; then
	echo "** Error: failed to update ${PORTALWEBAPP_CONTACT}"
	exit 1;
fi

echo "Restarting the tomcat9 service..."

service tomcat9 start

echo "** Customizing portal: done"
