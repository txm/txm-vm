#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`

echo "** Calling all Post-cloning TXM scripts"

if [ ! $# -eq 3 ]
  then
    echo "** Error: call with $# args. usage: 'vm_postclonage.sh MACHINE-NAME PORTAL-ADMIN-MAIL PORTAL-CONTACT-MAIL'"
    exit 1
fi

#PARAMETERS

MACHINE="$1"
PROJECT="$1"
ADMIN_MAIL="$2"
CONTACT_MAIL="$3"

sudo ./$SCRIPT_PATH/vm_customize_portal.sh $MACHINE $ADMIN_MAIL $CONTACT_MAIL

if [ $? != 0 ]; then
	echo "** Error: portal customization failed"
	exit 1;
fi

sudo ./$SCRIPT_PATH/vm_update_certificat.sh $MACHINE

if [ $? != 0 ]; then
        echo "** Error: HTTPS certificat update failed"
        exit 1;
fi

sudo ./$SCRIPT_PATH/vm_finalize_portaladmin.sh $MACHINE $ADMIN_MAIL $CONTACT_MAIL

if [ $? != 0 ]; then
        echo "** Error: portaladmin password update failed"
        exit 1;
fi

echo "Done: portal admin can be notified."
