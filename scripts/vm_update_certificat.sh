#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
echo "sourcing from $SCRIPT_PATH"
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

echo "** Updating certificat"

if [ ! $# -eq 1 ]
  then
    echo "** Error: call with $# args. usage: 'update_certificate.sh MACHINE-NAME'"
    exit 1
fi

MACHINE=$1

echo "** stop apache..."

sudo service apache2 stop

if [ $? != 0 ]; then
        echo "** Error: stop apache failed"
        exit 1;
fi

if is_backed_file "${APACHE_DEFAULT_CONF}"; then
	echo "** ${APACHE_DEFAULT_CONF} has already been modified. Remove ${APACHE_DEFAULT_CONF}.back or call vm_restore_postclonage.sh"
	exit 1
fi

if is_backed_file "${APACHE_DEFAULT_LE_CONF}"; then
	echo "** ${APACHE_DEFAULT_LE_CONF} has already been modified. Remove ${APACHE_DEFAULT_LE_CONF}.back or call vm_restore_postclonage.sh"
	exit 1
fi

if is_backed_file "${APACHE_DEFAULT_LE_CONF2}"; then
	echo "** ${APACHE_DEFAULT_LE_CONF2} has already been modified. Remove ${APACHE_DEFAULT_LE_CONF2}.back or call vm_restore_postclonage.sh"
	exit 1
fi

echo "** Updating server name - ensure server name is updated"

backup_file "${APACHE_DEFAULT_CONF}" &&

search_replace_check "${APACHE_DEFAULT_CONF}" "txm\.huma-num\.fr" "${MACHINE}\.huma-num\.fr"

if [ $? != 0 ]; then
        echo "** Error: ${APACHE_DEFAULT_CONF} update failed"
        exit 1;
fi


echo "** Updating server name in le-ssl ensure server name is updated"

backup_file "${APACHE_DEFAULT_LE_CONF}" &&

search_replace_check "$APACHE_DEFAULT_LE_CONF" "txm\.huma-num\.fr" "${MACHINE}\.huma-num\.fr"

if [ $? != 0 ]; then
        echo "** Error: sites/available/000-default-le-ssl.conf update failed"
        exit 1;
fi


echo "** Deleting le-sll link file"
backup_file "${APACHE_DEFAULT_LE_CONF2}" &&
sudo rm "${APACHE_DEFAULT_LE_CONF2}"

if [ $? != 0 ]; then
        echo "** Error: rm sites-enabled/000-default-le-ssl.conf failed"
        exit 1;
fi

echo "calling certbot, action choice: 2 to install&replace the cerficate"

sudo certbot --apache -d "$MACHINE.huma-num.fr" --no-redirect

if [ $? != 0 ]; then
        echo "** Error: certbot failed"
        exit 1;
fi


echo "** start apache..."

sudo service apache2 start

if [ $? != 0 ]; then
        echo "** Error: start apache failed"
        exit 1;
fi

echo "** Updating certificat: done"

