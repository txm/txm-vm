#!/bin/bash

SCRIPT_PATH=`dirname "$BASH_SOURCE"`
source $SCRIPT_PATH/tools.sh
source $SCRIPT_PATH/paths.sh

echo "** Undeploying current portal webapp."
sudo rm -rf $PORTALWEBAPP

sudo service tomcat9 restart
