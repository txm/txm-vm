# Docker build file of a TXM portal
# author: Jules Nuguet 2023-05-02

# The image is based on a Tomcat + Java 8 image


FROM tomcat:9.0-jre8-temurin-focal
WORKDIR /
RUN mkdir /usr/local/tomcat/webapps/txm
ADD /txm /usr/local/tomcat/webapps/txm

RUN mkdir /usr/local/tomcat/webapps/txm/TXMWEB
ADD /TXMWEB /TXMWEB

RUN ln -s /TXMWEB /root/TXMWEB

RUN apt-get update && apt-get install -y \
    sudo\
    libgtk2-perl \
    libgtk3-perl \
    libblas3 \
    libcairo2 \
    libgfortran4 \
    libgfortran5 \
    libglib2.0-0 \
    libgomp1 \
    libjpeg8 \
    liblapack3 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libpaper-utils \
    libreadline8 \
    libtiff5 \
    libx11-6 \
    libxt6 \
    tcl8.6 \
    tk8.6 \
    unzip \
    xdg-utils \
    zip

ADD TXM_0.7.9_Linux64_for_portal.deb /root
RUN dpkg -i /root/TXM_0.7.9_Linux64_for_portal.deb

RUN rm /TXMWEB/txm/data/users/*
RUN echo "default_admin_pass=21232f297a57a5a743894a0e4a801fc3" >> /TXMWEB/txm/txmweb.conf

EXPOSE 8080
CMD ["catalina.sh", "run"]
