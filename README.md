# TXM portal v0.6.3 for Ubuntu 18.04, 20.04 and 22.04

TXM portal software releases repository for Ubuntu, including: release **WAR**s, default **configuration files** and **administration scripts**.

## Contents

* txm/<br/>&nbsp;&nbsp;&nbsp;J2EE TXM portal WAR directory (to deploy with Tomcat)
* TXMWEB/txm/<br/>&nbsp;&nbsp;&nbsp;default configuration files
* scripts/<br/>&nbsp;&nbsp;&nbsp;portal management scripts and utilities
* doc/
  * Manuel du portail TXM 0.6.3 - installation simplifiée<br/>&nbsp;&nbsp;&nbsp;- portal installation manual (same content as the 'Install' section of this file)
  * Manuel du portail TXM 0.6.3 - prise en main<br/>&nbsp;&nbsp;&nbsp;- portal quick start guide
  * Manuel du portail TXM 0.6.3 - admin<br/>&nbsp;&nbsp;&nbsp;- portal administrator reference manual
* yourportal/<br/>&nbsp;&nbsp;&nbsp;**NOT USED YET**
* Dockerfile<br/>&nbsp;&nbsp;&nbsp;**NOT USED YET**

## Install

1. install dependencies and TXM 0.7.9 desktop software packaged for Ubuntu 18.04, 20.04 and 22.04

```shell
sudo apt install openjdk-8-jdk tomcat9 tomcat9-admin tomcat9-docs

sudo dpkg -i TXM_0.7.9_Linux64_for_portal.deb    # add '--force-all' for a Ubuntu 22.04 install

sudo cp /lib/x86_64-linux-gnu/libreadline.so.8 /lib/x86_64-linux-gnu/libreadline.so.6

sudo cp /lib/x86_64-linux-gnu/libreadline.so.7 /lib/x86_64-linux-gnu/libreadline.so.6
```
2. set the TOMCATHOME variable in the `paths.sh` file with the value defined in the `/usr/lib/sysusers.d/tomcat9.conf` file

3. make the `$TOMCATHOME/TXMWEB/txm` directory writable by the tomcat service, launch editor:

```shell
sudo systemctl edit --full tomcat9.service
```
add in the 'Security' section:
```
	ReadWritePaths=/var/lib/tomcat9/webapps/
	ReadWritePaths=<the TOMCATHOME VALUE>/TXMWEB/txm
```

4. download and extract the TXM Portal 0.6.3 release archive: https://gitlab.huma-num.fr/txm/txm-vm/-/archive/v0.6.3/txm-vm-v0.6.3.zip

5. set your working directory to the root of the extracted folder

```shell
cd [...]/txm-vm-v0.6.3
```

6. run the `portal_install.sh` script to install the web app and its default configuration files: this should load the "txm" web app from the '`txm/`' folder into your Tomcat instance "webapps" directory and its configuration files from the '`TXMWEB/txm/`' folder
```shell
sudo ./scripts/portal_install.sh
```

7. run the `vm_customize_portal.sh` script to quickly set the portal preference files and pages : this should set the portal web pages with the "txm" portal name, and the admin and contact emails (unless you are in the Huma-Num VM context, you can safely use the same email for 'admin' and 'contact' emails).
```shell
sudo ./vm_customize_portal.sh txm admin-contact@mail portal-contact@mail
```
8. set the "admin" portal special user temporary password
   1. generate the MD5 code of your password
	```shell
	/usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar makePassword.jar
	```
   2. stop the portal and delete user files
    ```shell
	sudo service tomcat9 stop
	sudo rm $TOMCATHOME/TXMWEB/txm/data/users/*
	```
   3.  set the admin default password in the portal configuration file
	```shell
	sudo nano $TOMCATHOME/TXMWEB/txm/txmweb.conf
	```
   4.  restart the portal, you can now connect with the "admin" portal user
	```shell
	sudo service tomcat9 start
	```
9. open the portal with a browser at `https://127.0.0.1:8080/txm`

10. login as admin with the temporary password

11. change the admin password to its definitive value in the user profile form
   
12. go to the administration panel and the "corpora" tab to load your first corpus

13. finalize & customize the portal configuration as described in the
'Manuel du portail TXM 0.6.3 - prise en main' manual

## Update

If your portal is hosted on huma-num, contact us. Else you have to replace the war file of your tomcat webapps:

1. Extract the new portal release files

2. Run the scripts/portal_update.sh script:
```shell
sudo ./scripts/portal_update.sh SOFTWARE
```
(the script stops tomcat, replaces the 'WEB-INF' & 'txmweb' content of the deployed war and starts tomcat)
3. Update is done.