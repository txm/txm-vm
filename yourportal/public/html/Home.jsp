<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
  <title>TXM demo portal Welcome</title>  
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta content="Serge Heiden" name="author" />
  <link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
</head>
<body>
<div class="textzone">

<h1>Bienvenue au portail http://${machine}.huma-num.fr/txm</h1>
<h3>Introduction</h3>
<p>
Ce portail TXM nu héberge deux corpus exemple :
<ul>
<li>
	TDM80J : Le tour du monde en quatre vingt jours, Jules Verne, 1873, 71 927 mots, 1 texte
	<ul>
	<li>ce corpus contient une édition synoptique affichant côte-à-côte l’édition TEI du texte et les images du fac-similé depuis le site Wikisource : <a class="command" command="edition" path="/TDM80J" editions="default" pageid="1">ouvrir l’édition</a></li>
	<li>la transcription <a href="https://fr.wikisource.org/wiki/Le_Tour_du_monde_en_quatre-vingts_jours">Wikisource</a> est dans le domaine public, le texte source XML-TEI P5 et le <a class="command" command="download" path="/TDM80J">corpus binaire TXM</a> sont sous licence CC BY-NC-SA 3.0</li>
	<li>ouvrir la <a class="command" command="documentation" path="/TDM80J">page d’accueil</a></li>
	</ul>
</li>
<li>
	VOEUX : Voeux présidentiels français 1959-2009, 54 716 mots, 51 discours
	<ul>
	<li>ce corpus est le support de la <a href="http://textometrie.ens-lyon.fr/html/enregistrement_atelier_initiation_TXM_fr.html">formation initiale à TXM</a></li>
	<li>les transcriptions source sont Copyright © 2010 Jean-Marc Leblanc, le <a class="command" command="download" path="/VOEUX">corpus binaire TXM</a> est sous licence CC BY-NC-SA 3.0</li>
	<li>ouvrir la <a class="command" command="documentation" path="/VOEUX">page d’accueil</a></li>
	</ul>
</li>
</ul>
</p>
<p>
Ces deux corpus sont lemmatisés et étiquetés morpho-syntaxiquement par TreeTagger.
</p>
<p>
Vous pouvez vérifier le bon fonctionnement de ces corpus, et de ce portail, en cliquant sur les liens donnés en exemple dans leur page d’accueil respective :
<ul>
	<li>page d’accueil du <a class="command" command="documentation" path="/TDM80J">corpus TDM80J</a></li>
	<li>page d’accueil du <a class="command" command="documentation" path="/VOEUX">corpus VOEUX</a></li>
</ul>
</p>

<h3>Ce qu’il reste à faire</h3>
<p>
<ol>
	<li>il vous reste à configurer le portail et à y ajouter vos propres corpus en suivant la « procédure d’installation d’un portail web TXM ».</li>
	<li>puis à changer le contenu de cette page d’accueil, et éventuellement retirer les deux corpus exemple, avant la mise en production.</li>
</ol>
</p>

<h3>Contact</h3>
<p>
N’hésitez pas à <a href="mailto:textometrie@groupes.renater.fr?subject=[support]%20portail%20%3Chttp%3A%2F%2F%24%7Bmachine%7D.huma-num.fr%2Ftxm%3E&amp;body=%20">contacter l’équipe TXM</a> <i>textometrie AT groupes.renater.fr</i> en cas de difficulté d’installation.
</p>

</div>
</body></html>


