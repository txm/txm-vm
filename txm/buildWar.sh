#!/bin/sh

if [ $# -ne 1 ]    # Script invoked with no command-line args?
then
  echo "Usage: sh buildWar <warname>"
  echo "ex: sh buildWar test"
  exit 1
fi  
NAME="$1"
NEWPORTAL="$2"

LOGIN="txm"
URL="txm.ish-lyon.cnrs.fr"
REMOTEPATH="/var/lib/tomcat6/txm/data/projets"

echo "** TXMWEB build war: start building war $NAME.war"
if [ -f "$NAME.war" ]; then
	echo "** TXMWEB build war: can't use $NAME.war for war name, a file with the same name already exists"
	exit 1;
fi

echo "** TXMWEB build war: listing war content"
rm list
find * | grep -v ".svn" | grep -v "*.war" > list &&
sed -i 's/ /\\ /g' list
if [ $? != 0 ]; then
	echo "** TXMWEB build war: Failed to war file list content."
	exit 1;
fi
cat list

echo "** TXMWEB: removing demo files from war files"
sed -i 's/html\/.*//g' list

echo "** TXMWEB build war: creating war file: $NAME.war"
zip -q $NAME.war `cat list` css/TXM\ WEB.css css/TXM\ WEB\ PRINT.css
if [ $? != 0 ]; then
	echo "** TXMWEB build war: Failed to zip $NAME content."
	exit 1;
fi

# txm.war is created to build portal release on SF
if [ "txm" != $NAME ]; then
	echo "** TXMWEB build war: sending $NAME.war to $LOGIN@$URL:$REMOTEPATH"
	scp "$NAME.war" $LOGIN@$URL:$REMOTEPATH
	if [ $? != 0 ]; then
		echo "** TXMWEB build war: Failed to send $NAME.war"
		exit 1;
	fi
fi
echo "** TXMWEB build war: done !"

