<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
</head>
<body>
<div class="main">
<div class="textzone">
<h1>Votre compte est en attente de validation...</h1>
<p>Vous ne pouvez pas accéder à la totalité des corpus de la plateforme tant qu'un administrateur n'a pas
validé votre compte. Vous serez informé par e-mail dès que ce sera le cas.</p>
<p>Si vous ne recevez pas le mail de validation de votre compte 7 jours après l'envoi du formulaire, merci de contacter l'administrateur</p>
<h1>Gérer mes préférences :</h1>
<p>Vous pouvez, en attendant, gérer vos préférences.</p>
<p><ul>
<li>Accéder à mes préférences : <span style="font-weight: bold;" id="profile_target" type="link"/></li>
</ul></p>
<h1>Les outils accessibles sans inscription :</h1>
<p>Vous avez accès au corpus <i>DISCOURS</i> : vous pouvez ainsi tester les différents outils de la plateforme</p>
<p>Pour en savoir plus sur les fonctionnalités de la plateforme BFM, vous pouvez consulter :
<ul><li>le <a href="http://sourceforge.net/projects/textometrie/files/documentation/Manuel%20de%20Reference%20TXM%200.5_FR.odt/download" target="_blank">
Manuel de Référence TXM</a> (qui vous présentera tous les outils de TXM)</li>
<li>le tutoriel BFM (qui vous présentera un exemple de session de travail)</li></ul></p>
</div>
</div>
</body>
</html>
