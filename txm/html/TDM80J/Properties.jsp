<jsp:directive.page
        contentType="text/html;charset=UTF-8" />
<? version="1.0" encoding="UTF-8"?>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/></head><body>
<h2 style'font-family:"Arial";'>Propriétés de TDM80J (CQP ID=TDM80J)</h2>
<h3 style'font-family:"Arial";'>Statistiques Générales</h3>
<ul>
<li>Nombre de mots : 85 130</li>
<li>Nombre de propriétés de mot 5 (word, frlemma, frpos, type, n)</li>
<li>Nombre d'unités de structure 10 (body, cell, div, head, hi, hi1, p, row, table, text)</li>
</ul>
<h3 style'font-family:"Arial";'>Propriétés des unités lexicales (max 100 valeurs)</h3>
<ul>
<li> frlemma : chapitre, I, ., dans, lequel, Phileas, Fogg, et, Passepartout, se, accepter, réciproquement, le, un, comme, maître, ,, autre, domestique, En, année, @card@, maison, porter, numéro, de, Saville-row, Burlington, Gardens, –, Sheridan, mourir, en, être, habiter, par, esq, du, membre, plus, singulier, remarquer, Reform-Club, Londres, bien, que, il, sembler, prendre, à, tâche, ne, rien, faire, qui, pouvoir, attirer, attention, grand, orateur, honorer, Angleterre, </li>
<li> frpos : NOM, NUM, SENT, PRP, PRO:REL, NAM, KON, PRO:PER, VER:pres, ADV, DET:ART, PUN, ADJ, VER:ppre, VER:simp, VER:impf, VER:pper, PRP:det, VER:subi, VER:infi, </li>
<li> n : 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, ...</li>
<li> type : w, pon, </li>
<li> word : Chapitre, I, ., Dans, lequel, Phileas, Fogg, et, Passepartout, s’, acceptent, réciproquement, l’, un, comme, maître, ,, autre, domestique, En, année, 1872, la, maison, portant, le, numéro, 7, de, Saville-row, Burlington, Gardens, –, dans, laquelle, Sheridan, mourut, en, 1814, était, habitée, par, esq, des, membres, les, plus, singuliers, remarqués, du, Reform-Club, Londres, bien, qu’, il, semblât, prendre, à, tâche, ne, rien, faire, qui, pût, attirer, attention, À, grands, orateurs, honorent, Angleterre, </li>
</ul>
<h3 style'font-family:"Arial";'>Propriétés des structures (max 100 valeurs)</h3>
<ul>
<li> body
<ul>
<li> n (1) = 0.</li>
</ul>
</li>
<li> cell
<ul>
<li> cols (1) = 1.</li>
<li> n (1) = "".</li>
<li> rend (2) = "", table-cell-align-right.</li>
<li> role (1) = data.</li>
<li> rows (1) = 1.</li>
</ul>
</li>
<li> div
<ul>
<li> id (37) = div1, div10, div11, div12, div13, div14, div15, div16, div17, div18, div19, div2, div20, div21, div22, div23, div24, div25, div26, div27, div28, div29, div3, div30, div31, div32, div33, div34, div35, div36, div37, div4, div5, div6, div7, div8, div9.</li>
<li> n (1) = "".</li>
<li> org (1) = uniform.</li>
<li> part (1) = N.</li>
<li> sample (1) = complete.</li>
<li> type (1) = chapter.</li>
</ul>
</li>
<li> head
<ul>
<li> n (37) = 0, 1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 30, 31, 32, 33, 34, 35, 36, 4, 5, 6, 7, 8, 9.</li>
</ul>
</li>
<li> hi
<ul>
<li> n (1) = "".</li>
<li> n1 (1) = "".</li>
<li> rend (3) = color_996600, i, sup.</li>
<li> rend1 (1) = b.</li>
</ul>
</li>
<li> p
<ul>
<li> n (1) = "".</li>
<li> part (1) = N.</li>
<li> rend (3) = "", center noindent, right.</li>
</ul>
</li>
<li> row
<ul>
<li> cols (1) = 1.</li>
<li> n (1) = "".</li>
<li> role (1) = data.</li>
<li> rows (1) = 1.</li>
</ul>
</li>
<li> table
<ul>
<li> n (1) = 0.</li>
</ul>
</li>
<li> text
<ul>
<li> auteur (1) = Jules Verne.</li>
<li> date (1) = 1873.</li>
<li> id (1) = tdm80j.</li>
<li> language (1) = fr.</li>
<li> licence (1) = Texte libre de droits.</li>
<li> titre (1) = Le tour du monde en quatre vingt jours.</li>
</ul>
</li>
</ul>
</body>
</html>
