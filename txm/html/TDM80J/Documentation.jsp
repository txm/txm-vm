<jsp:directive.page
	contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="fr" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Bienvenue à la page d'accueil du corpus TDM80J</title>
		<meta name="generator" content="Bluefish 2.2.2" />
		<meta http-equiv="Content-Type"
			content="text/html; charset=utf-8" />
		<meta content="Serge Heiden" name="author" />
		<link href="css/TXM%20WEB_TDM80J.css" rel="stylesheet"
			type="text/css" media="screen" />
	</head>
	<body>
		<div class="textzone" style="background: no-repeat right 5% top 280px url('html/TDM80J/splash.jpg');">

			<h1>Bienvenue à la page d'accueil du corpus TDM80J</h1>

			<p>
				Le corpus TDM80J est composé du texte « Le tour du monde en quatre
				vingt jours » de Jules Verne, soit 85 130 mots.
				<br />
				Il s'agit de l'édition
				<a
					href="https://fr.wikisource.org/wiki/Le_Tour_du_monde_en_quatre-vingts_jours"
					target="_blank">J. Hetzel et Compagnie, 1873</a>,
				numérisée par Wikisource <a href="https://fr.wikisource.org" target="_blank"><img src="images/wikisource_151534.png"/></a> et encodée en <a href="https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/index.html"		target="_blank">XML-TEI P5 <img src="images/logo-TEI.jpg" width="22"/></a> par Serge Heiden
				pour le projet Textométrie <a href="https://textometrie.org" target="_blank"><img src="images/txm-icon-48x48.jpg" width="22"/></a>.
			</p>

			<h2>Édition <img src="images/icons/functions/Edition.png"/></h2>

			<p>Le texte possède une édition synoptique affichant côte-à-côte : à
				gauche, l’édition TEI du texte incluant les images des illustrations
				du livre ; à droite, les images du fac-similé du livre du site
				Wikisource.</p>

			<ul>
				<li>
					<p>
						ouvrir l'édition à la
						<a class="command" command="edition" path="/TDM80J"
							editions="default,facs" textid="tdm80j" pageid="2">page 1</a>
						;
					</p>
				</li>
				<li>
					<p>
						ouvrir l'édition à la page 1 en mettant en évidence la première
						occurrence de la séquence «
						<a class="command" command="edition" path="/TDM80J"
							textid="tdm80j" editions="default,facs"
							wordids="w_tdm80j_6,w_tdm80j_7">Phileas Fogg</a>
						».
					</p>
				</li>
			</ul>

			<h2>Enrichissement linguistique</h2>

			Le texte a été lemmatisé et étiqueté morpho-syntaxiquement avec
			TreeTagger.

			<h2>Raccourcis d'exploration</h2>

			<p>
				Voici quelques liens d'accès direct à des exemples de calculs sur le
				corpus :
				<br />
				<ul>
					<li>
						<p>
							calculer la
							<a class="command" command="description" path="/TDM80J">taille</a>
							du texte <img src="images/icons/functions/Propriétés.png"/> ;
						</p>
						<b>Lexique</b> <img src="images/icons/functions/VocabularyP.png"/> et <img src="images/icons/functions/Vocabulary.png"/>
						(double-cliquer sur une ligne pour calculer la concordance d'un
						mot)
			</p>
		</li>
			<li>
				<p>
					<a title="commande Lexique" class="command" command="lexicon"
						path="/TDM80J">lexique</a>
					hiérarchique complet (trié par fréquence décroissante) ;
				</p>
			</li>
			<li>
				<p>
					<a title="commande Index" class="command" command="index"
						path="/TDM80J" query='[frpos="NOM"]'>lexique des noms communs</a>
					(tous les lexiques suivants sont calculés à partir de la commande
					Index) ;
				</p>
			</li>
			<li>
				<p>
					<a title="commande Index" class="command" command="index"
						path="/TDM80J" query='[frpos="NAM"]'>lexique des noms propres</a>
					;
				</p>
			</li>
			<li>
				<p>
					<a title="commande Index" class="command" command="index"
						path="/TDM80J" query='[frpos="ADJ"]'>lexique des adjectifs</a>
					;
				</p>
			</li>
			<li>
				<p>
					<a title="commande Index" class="command" command="index"
						path="/TDM80J" query='[frpos="VER.*"]'>lexique des verbes</a>
					;
				</p>
			</li>
			<li>
				<p>
					<a title="commande Index" class="command" command="index"
						path="/TDM80J" query='[frpos="ADV"]'>lexique des adverbes</a>
					;
				</p>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="VER:cond"]'>lexique des verbes au conditionnel</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="VER:futu"]'>lexique des verbes au futur</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J"
							query='[frpos="VER:impf|VER:simp|VER:pper|VER:simp"]'>lexique des verbes au passé</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="VER:impf"]'>lexique des verbes à l'imparfait</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="VER:infi"]'>lexique des verbes à l'infinitif</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="PRO.*"]'>lexique des pronoms</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="PRO:PER"]'>lexique des pronoms personnels</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="PRO:POS"]'>lexique des pronoms possessifs</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="PRO:REL"]'>lexique des pronoms relatifs</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="PRP.*"]'>lexique des prépositions</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="KON"]'>lexique des conjonctions</a>
						;
					</p>
				</li>
				<li>
					<p>
						<a title="commande Index" class="command" command="index"
							path="/TDM80J" query='[frpos="INT"]'>lexique des interjections</a>
						;
					</p>
				</li>

				<b>Concordance <img src="images/icons/functions/Concordances.png"/></b>
			</p>
			</li>
			<li>
				<p>calculer la concordance :</p>
				<ul>
					<li>
						<p>
							d'un mot : « 
							<a class="command" command="concordance" path="/TDM80J"
								query='Fogg'>Fogg</a>
							 » (double-cliquer sur une ligne de concordance pour lire
							l'occurrence du mot pivot en plein texte dans l'édition) ;
						</p>
					</li>
					<li>
						<p>
							d'une séquence de mots : « 
							<a class="command" command="concordance" path="/TDM80J"
								query='[word="Phileas"] [word="Fogg"]'>Phileas Fogg</a>
							 » (double-cliquer sur une ligne de concordance pour lire
							l'occurrence de la séquence en plein texte dans l'édition) ;
						</p>
					</li>
					<li>
						<p>
							d'une séquence de deux mots ou d'un mot particulier : « 
							<a class="command" command="concordance" path="/TDM80J"
								query='([word="Phileas"] [word="Fogg"])|([word="Passepartout"])'>Phileas Fogg | Passepartout</a>
							 » (double-cliquer sur une ligne de concordance pour lire
							l'occurrence des mots en plein texte dans l'édition) ;
						</p>
					</li>
					<li>
						<p>
							d'un début de mot : « 
							<a class="command" command="concordance" path="/TDM80J"
								query='voyag.*'>voyag.*</a>
							 » ;
						</p>
					</li>
					<li>
						<p>
							d'un lemme particulier : « 
							<a class="command" command="concordance" path="/TDM80J"
								query='[frlemma="voyager"]'>[frlemma="voyager"]</a>
							 » ;
						</p>
					</li>
					<li>
						<p>
							d'une combinaison de lemme et de partie du discours : « 
							<a class="command" command="concordance" path="/TDM80J"
								query='[frlemma="devoir" & frpos="NOM"]'>[frlemma="devoir" & frpos="NOM"]</a>
							 » ;
						</p>
					</li>
				</ul>
				<b>Contextes <img src="images/icons/functions/Contexts.png"/></b>
			</p>
			</li>
			<li>
				<p>calculer les contextes d'un mot : « 
							<a class="command" command="context" path="/TDM80J"
								query='Fogg' prop="word">Fogg</a>
							 » ;
				</p>
				<b>Index</b> <img src="images/icons/functions/Vocabulary.png"
			</li>
			<li>
				<p>
					calculer la fréquence du mot « 
					<a class="command" command="index" path="/TDM80J" query='jour'>jour</a>
					 » (son nombre d'apparitions) ;
				</p>
			</li>
			<li>
				<p>
					lister les réalisations du radical
					<a class="command" command="index" path="/TDM80J"
						query='.*patri.*'>.*patri.*</a>
					;
				</p>
			</li>
			<li>
				<p>
					lister
					<a class="command" command="index" path="/TDM80J"
						query='[word="pays"] [frpos="ADJ"]'>comment est qualifié un pays</a>
					(index de [word="pays"] [frpos="ADJ"]) ;
				</p>
			</li>
			<li>
				<p>
					lister
					<a class="command" command="index" path="/TDM80J"
						query='[frpos="NOM"] [word="anglais.*"]'>ce qui est anglais</a>
					(index de [frpos="NOM"] [word="anglais.*"]) ;
				</p>
			</li>
			<li>
				<p>
					lister
					<a class="command" command="index" path="/TDM80J"
						query='[frpos="NOM"] [word="français.*"]'>ce qui est français</a>
					(index de [frpos="NOM"] [word="français.*"]) ;
				</p>
			</li>
			<li>
				<p>
					lister les
					<a class="command" command="index" path="/TDM80J"
						query='[frlemma="air"] [frpos="ADJ"]'>différents airs</a>
					(index de [frlemma="air"] [frpos="ADJ"]) ;
				</p>
			</li>
			<li>
				<p>
					lister des séquences de mots comprenant
					<a class="command" command="index" path="/TDM80J"
						query='[frlemma="ne"][frlemma!="pas"]{1,3}[frlemma="pas"]'>des insertions ou des discontinuités</a>
					(index de [frlemma="ne"][frlemma!="pas"]{1,3}[frlemma="pas"]) ;
				</p>
				<br />
				<br />
				<b>Téléchargement</b>
			</li>
			<li>
				<p>
					télécharger le
					<a class="command" command="download" path="/TDM80J">corpus binaire <img src="images/icons/download.png"/></a>
					, à utiliser dans l'application
					<a
						href="https://txm.gitpages.huma-num.fr/textometrie/files/software/TXM/0.8.1"
						target="_blank">TXM pour poste <img src="images/txm-icon-48x48.jpg" width="22"/></a>
				</p>
			</li>
		</ul>
		</p>

			<hr />

			<p>
				Copyright © 2020 Serge Heiden. Licensed under Creative Commons
				Attribution-NonCommercial-ShareAlike 3.0 Unported
				<br />

				Veuillez nous contacter pour plus d’informations.
			</p>

		</div>

	</body>
</html>

