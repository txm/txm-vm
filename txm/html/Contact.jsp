<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
  <title>Administrator contact information</title>
  <meta content="Serge Heiden" name="author" />
</head><body>
<div class="textzone">
<h1 style="font-family: Helvetica,Arial,sans-serif;">Contact the administrator of this portal</h1>
<p>
Please send an email to '<span style="font-weight: bold; font-family: monospace;">textometrie AT ens-lyon DOT fr</span>'.
</p>
</div>
</body></html>
