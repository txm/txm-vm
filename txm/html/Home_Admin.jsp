<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
</head>

<body>

<div class="main">
<div class="textzone">

<h1>Tests après redémarrage du portail </h1>
<ol>
	<li>Se connecter au portail</li>
	<li>Vérifier la présence des corpus : BROWN, VOEUX, GRAAL, etc</li>
	<li>Lancer la sélection sur le corpus BROWN : <a class="internal" command="selection" type="link" path="/DISCOURS" query="">Selection dans BROWN</a></a></li>
	<li>Lancer un lexique sur le corpus BROWN : <a class="command" command="lexicon" type="link" path="/DISCOURS" query="I">Lexique de 'I' dans BROWN</a></a><p></p></li>
</ol>

<h1>Instructions pour gérer la plateforme :</h1>
<p>
<ul>
<li>Cliquez sur le bouton "Administrer" pour accéder aux outils</li>
<li>Dans l'onglet "Profils", vous pouvez éditer les profils, les supprimer et en créer 
de nouveaux</li>
<li>Dans l'onglet "Utilisateurs", vous pouvez attribuer ou enlever un profil à un utilisateur, 
importer ou exporter des utilisateurs, changer les informations attachées à un utilisateur</li>
<li>Dans l'onglet "Bases", vous pouvez charger une base</li>
</ul>
</p>
</div>
</div>

</body>
</html>
