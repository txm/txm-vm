<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
  <title>TXM demo portal Welcome</title>  
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta content="Serge Heiden" name="author" />
  <link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
</head>
<body>
<div class="textzone">
<h1 style="padding-left: 25px; font-family: Helvetica,Arial,sans-serif;">Welcome
to TXM demo portal</h1>
<p>This site allows you to experiment with the TXM web platform on
various public and private corpora.</p>
<ul>
  <li><span style="font-weight: bold;">public</span> corpora are always accessible through their respective icon in the Corpus Explorer (the left panel) ;</li>
  <li><span style="font-weight: bold;">private</span> corpora are accessible only when connected to the portal with a login given to you by the portal administrator.</li>
</ul>
<p>To access a public corpus, right-click on its icon and select a command to apply (Lexicon, Concordance, etc.).<br />
See <span style="font-weight: bold;" id="help_target" type="link">Help</span> for documentation of the commands.<br />
</p>
<p>To access private corpora, you have to <span style="font-weight: bold;" id="cmd_connexion">login</span> first.
</p>
<p>For any other information, please <span id="cmd_contact" type="link">contact
the administrator</span>.
</p>
</div>
</body></html>
