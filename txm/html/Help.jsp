<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>Help &amp; Links</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
	<meta content="Serge Heiden" name="author" />
</head>
<body>
<div class="textzone">
<h1 style="font-family: Helvetica,Arial,sans-serif;">Help &amp; Links</h1>
<p>
There is as yet no manual specifically for TXM GWT (web portal).<br />
Its interface is nevertheless sufficiently similar to TXM RCP to allow you<br />
to use the manual made for the TXM RCP version.
</p>
<ul>
  <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/TXMReferenceManual0.5_EN.xhtml">TXM 0.5 Reference Manual</a> (Online)</li><br />
  <ul>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/TXMReferenceManual0.5_EN17.xhtml#toc52">Create sub-corpus</a></li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/TXMReferenceManual0.5_EN18.xhtml#toc56">Create partition</a></li>
    <li>Text selection : new interface to create sub-corpus (not documented yet)</li><br />
    <li>Properties : give the size of the corpus - number of words &amp; number of types</li>
    <li>References : list bibliographic references of matches</li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/TXMReferenceManual0.5_EN21.xhtml#toc71">Index</a></li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/TXMReferenceManual0.5_EN21.xhtml#toc70">Lexicon</a></li>
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/TXMReferenceManual0.5_EN19.xhtml#toc60">Concordance</a></li>
    <li>Context : like 'Concordance' but without vertical alignment of keywords</li>
    <li>Tiger Search : syntactic queries available only if syntactic annotation has been loaded with the corpus</li><br />
    <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/TXMReferenceManual0.5_EN16.xhtml#toc49">Text view</a></li>
    <li>bibliographic records : available only if bibliographic records have been loaded with the corpus</li>
    <li>Delete : delete the sub-corpus or partition</li>
  </ul>
<br />
  <li><a target="HelpTab" href="http://textometrie.sourceforge.net/doc/refman/ManueldeReferenceTXM0.5_FR.xhtml">Manuel de référence TXM 0.5</a> (en ligne)</li>
  <li><a target="HelpTab" href="http://sourceforge.net/projects/textometrie/files/documentation/TXM%20Reference%20Manual%200.5_EN.pdf/download" title="Click to download TXM Reference Manual 0.5_EN.pdf" class="name">TXM Reference Manual 0.5</a> (PDF for printing)
  <li><a target="HelpTab" href="http://sourceforge.net/projects/textometrie/files/documentation/Manuel%20de%20Reference%20TXM%200.5_FR.pdf/download">Manuel de référence TXM 0.5</a> (PDF pour impression)</li><br />
  <li><a target="HelpTab" href="https://sourceforge.net/projects/textometrie/files/documentation">Autres manuels</a></li>
  <li><a target="HelpTab" href="http://textometrie.ens-lyon.fr/spip.php?article96">Documentation du projet Textométrie</a></li>
  <li><a target="HelpTab" href="http://textometrie.ens-lsh.fr/IMG/html/intro-discours.htm">Tutoriel vidéo introductif</a> (TXM RCP version 0.4.6)</li>
  <li><a target="HelpTab" href="https://listes.cru.fr/wiki/txm-users">Wiki des utilisateurs de TXM</a></li>
  <li>Liste de diffusion francophone&nbsp;: <a target="HelpTab" href="https://listes.cru.fr/sympa/subscribe/txm-users">txm-users AT cru.fr</a></li>
  <li>International mailing list : <a target="HelpTab" href="http://lists.sourceforge.net/mailman/listinfo/textometrie-open">textometrie-open AT lists.sourceforge.net</a></li>
<br />
  <li><a target="HelpTab" href="http://sourceforge.net/apps/mediawiki/textometrie">Wiki des développeurs de TXM</a></li>
  <li><a target="HelpTab" href="http://textometrie.sourceforge.net/javadoc/index.html">Javadoc des sources de TXM</a></li>
<br />
  <li><a target="HelpTab" href="http://textometrie.ens-lyon.fr/spip.php?article62">Publications du projet Textométrie</a></li>
</ul>

</div>
</body></html>
