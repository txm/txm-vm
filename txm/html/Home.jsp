<jsp:directive.page
	contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" lang="fr">
	<head>
		<title>Bienvenue au portail MACHINE</title>
		<meta http-equiv="Content-Type"
			content="text/html; charset=UTF-8" />
		<meta content="Serge Heiden" name="author" />
		<link href="css/TXM WEB.css" rel="stylesheet" type="text/css"
			media="screen" />
		<link href="css/TXM WEB PRINT.css" rel="stylesheet"
			type="text/css" media="print" />
	</head>
	<body>
		<div class="textzone">

			<h1>Bienvenue au portail MACHINE</h1>

			<h2>Introduction</h2>

			<p>
				Ce portail héberge deux corpus exemple :
				<ul>
					<li>
						TDM80J : corpus constitué du roman « Le tour du monde en quatre
						vingt jours » de Jules Verne (1873), 85 130 mots, 1 texte
						<ul>
							<li>il est représentatif d'un « grand » texte analysable comme
								corpus</li>
							<li>
								il comprend une
								<a class="command" command="edition" path="/TDM80J" textid="tdm80j" 
									editions="default,facs" pageid="2">édition synoptique <img class="valign" src="images/icons/functions/Edition.png"/></a>
								affichant côte-à-côte une édition de la transcription
								<a
									href="https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/index.html"
									target="_blank">TEI <img src="images/logo-TEI.jpg" width="22"/></a>
								du texte intégrant les illustrations d'origine et les images du
								fac-similé depuis le site Wikisource
							</li>
							<li>
								la transcription
								<a
									href="https://fr.wikisource.org/wiki/Le_Tour_du_monde_en_quatre-vingts_jours"
									target="_blank">Wikisource <img src="images/wikisource_151534.png"/></a>
								est dans le domaine public, le
								<a
									href="https://gitlab.huma-num.fr/txm/txm-ressources/-/tree/master/corpora/TDM80J/src/tdm80j"
									target="_blank">texte source XML-TEI P5</a>
								et le
								<a class="command" command="download" path="/TDM80J" share>corpus binaire TXM <img src="images/icons/download.png"/></a>
								sont sous licence CC BY-NC-SA 3.0
								<br />
								<b>→</b>
								ouvrir la
								<a class="command" command="documentation" path="/TDM80J">page d’accueil <img class="valign" src="images/icons/house.png"/></a>
							</li>
						</ul>
					</li>
					<li>
						VOEUX : corpus constitué de transcriptions des discours de voeux
						des présidents français de 1959 à 2012, 61 143 mots, 54 discours
						<ul>
							<li>il est représentatif d'un corpus analysable chronologiquement
								et par émetteurs</li>
							<li>
								ce corpus sert de support à la
								<a
									href="http://textometrie.ens-lyon.fr/html/enregistrement_atelier_initiation_TXM_fr.html" class="valign"
									target="_blank">formation initiale à TXM <img src="images/eclass-candi-logo.png" width="22"/></a>
							</li>
							<li>
								les transcriptions source sont Copyright © 2010 Jean-Marc
								Leblanc, le
								<a class="command" command="download" path="/VOEUX">corpus binaire TXM <img src="images/icons/download.png"/></a>
								(à charger dans un TXM pour poste) est sous licence CC BY-NC-SA
								3.0
								<br />
								<b>→</b>
								ouvrir la
								<a class="command" command="documentation" path="/VOEUX">page d’accueil <img class="valign" src="images/icons/house.png"/></a>
							</li>
						</ul>
					</li>
				</ul>
			</p>
			<p>
				Ces deux corpus ont été automatiquement lemmatisés et étiquetés
				morpho-syntaxiquement avec le logiciel
				<a href="https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger"
					target="_blank">TreeTagger</a>.
			</p>
			<p>

				<h2>Vérification du bon fonctionnement de ce portail</h2>

				<p>Pour vérifier rapidement le bon fonctionnement de ce
					portail et de ses corpus, vous pouvez cliquer sur des <b>liens d'appel automatique de commandes</b> accessibles dans les pages d’accueil de chaque corpus.</p>

				<p>
					Pour cela vous pouvez,

					<ul>
						<li>
							soit :
							<ol>
 								<li>d'abord accéder à l'une des pages d'accueil de corpus par un lien ci-dessous :</li>
			
								<ul>
									<li>
									page d’accueil du corpus
									<a class="command" command="documentation" path="/TDM80J">TDM80J <img class="valign" src="images/icons/house.png"/></a>
									</li>
									<li>
									page d’accueil du corpus
									<a class="command" command="documentation" path="/VOEUX">VOEUX <img class="valign" src="images/icons/house.png"/></a>
									</li>
								</ul>
								<li>puis cliquer sur les liens d'appel de commandes proposés en exemples dans la page.</li>
							</ol>
						</li>
						<li>
							soit :
							<ol>
								<li> d'abord sélectionner l'une des icones de corpus <img class="valign" src="images/icons/corpus.png"/> « TDM80J » ou « VOEUX » dans la vue Corpus
									située à gauche</li>
								<li> puis cliquer sur le bouton de commande d'ouverture de page d'Accueil <img class="valign" src="images/icons/house.png"/> de la barre d'outils <img class="valign" src="images/toolbar.png" height="22"/> (la barre s'ouvre quand on sélectionne le corpus)</li>
								<li> vous pouvez alors cliquer sur les liens d'appel de commandes proposés en exemples dans la page.</li>
							</ol>
						</li>
					</ul>
				</p>
				<p>Les résultats des commandes s'affichent dans des onglets indépendants - internes à la page TXM,
					ce qui permet de naviguer facilement entre la page d'accueil et les différents résultats.</p>

				<h2>Ce qu’il reste à faire</h2>

				<p>Il vous reste :
					<ol>
						<li>
							à personnaliser ce portail en y ajoutant vos propres
							corpus en suivant les instructions de la section « 
							<a
								href="https://sharedocs.huma-num.fr/wl/?id=zgMQUvhYex3T2aTqxH6PL1sXBGXUOUKp"
								target="_blank">Prise en main rapide d'un portail TXM (Huma-Num)</a>
							 » du Manuel d'administration du logiciel portail web TXM 0.6.3.
						</li>
						<li> à changer le contenu de cette page d’accueil</li>
						<li> à retirer les deux corpus exemple, avant la mise en production.</li>
					</ol>
				</p>

				<h2>Documentation</h2>
				<p>
					Voir la section « 
					<a
						href="https://txm.gitpages.huma-num.fr/textometrie/Documentation/#documentation-pour-ladministrateur-du-logiciel-portail-txm"
						target="_blank">Documentation pour l’administrateur du logiciel portail TXM</a>
					» du site du projet Textométrie.
				</p>

				<h2>Contact</h2>
				<p>
					N’hésitez pas à
					<a
						href="mailto:textometrie@groupes.renater.fr?subject=[support]%20portail%20%3Chttp%3A%2F%2F%24%7Bmachine%7D.huma-num.fr%2Ftxm%3E&amp;body=%20">contacter l’équipe TXM <img class="valign" src="images/mailto_logo.png" width="22"/></a>
					<i>textometrie AT groupes.renater.fr</i>
					en cas de difficulté d’installation.
				</p>

				<p id="powered_line"  style="margin-Bottom:10px">
					Powered by
					<img src="images/logo_txm_lineaire_transparent.png" style="margin-Bottom:-4px"/>
				</p>

		</div>
	</body>
</html>



