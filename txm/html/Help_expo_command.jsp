<jsp:directive.page contentType="text/html;charset=UTF-8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>Help &amp; Links</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/TXM WEB.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/TXM WEB PRINT.css" rel="stylesheet" type="text/css" media="print" />
	<meta content="Serge Heiden" name="author" />
</head>
<body >
<div style="background-color:rgba(255, 0, 0, 0.2);width:100%;height:100%;">

<div style="position:relative;top:15%;left:5%;font-size:large;"><span style="font-size:xx-large;">⬉</span> 1- Cliquez sur un mot</div>

<div style="position:relative;top:35%;left:15%;font-size:large;">4- L'occurence sélectionnée est alors mise en évidence dans l'édition <span style="font-size:xx-large;">⬈</span></div>

<div style="position:relative;top:50%;left:5%;font-size:large;">2- Toutes les occurrences sont affichées ici <span style="font-size:xx-large;">⬊</span></div>
<div style="position:relative;top:60%;left:15%;font-size:large;">3- Pour une lecture complète, il suffit de cliquer sur une ligne <span style="font-size:xx-large;">⬈</span></div>

</div>
</body></html>
