<html>
<body>
    <div>
	<div style="font-size:200%;text-align:center"><h1>Queste del saint Graal</h1></div>
        <div>
        	<h2 style="text-align:center">
        		Édition numérique interactive <br>
				du manuscrit de Lyon (Bibliothèque municipale, P.A. 77)

      		</h2>	
      		<p style="text-align:center">	Edité par <b>Christiane Marchello-Nizia</b> et <b>Alexei Lavrentiev</b><br>
      			<br>

      		</p>
			<table align="center" style="border:none;width=auto;font-size:120%">
			<tr>
			<td style="padding:10px;border-style:outset;border-width:5px"><b>Accueil</a></td>
			<td style="padding:10px;border-style:outset;border-width:5px"><a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm_2013-07-intro.pdf" target="_blank">Introduction</a></td>
			<td style="padding:10px;border-style:outset;border-width:5px;font-size:150%;font-wight:bold"><a class="command" command="texts" path="/GRAAL">Édition</a></td>
			
<td style="padding:10px;border-style:outset;border-width:5px;font-size:150%;font-wight:bold"><a class="command" command="edition" path="/GRAAL" editions="fac-similaire2,courante2" textid="qgraal_cm">Édition</a></td>

			<td style="padding:10px;border-style:outset;border-width:5px"><a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm_2013-07-glossaire.pdf" target="_blank">Glossaire</a></td>
			<td style="padding:10px;border-style:outset;border-width:5px"><a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm_2013-07-inp.pdf" target="_blank">Index des noms propres</a></td>
			<td style="padding:10px;border-style:outset;border-width:5px"><a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm_2013-07.pdf" target="_blank">PDF</a></td>
			<td style="padding:10px;border-style:outset;border-width:5px"><a href="http://txm.ish-lyon.cnrs.fr/bfm/files/graal_src.zip" target="_blank">Sources XML-TEI</a></td>
			<td style="padding:10px;border-style:outset;border-width:5px"><a class="internal" path="/GRAAL/Graal-Mentions-Légales">Mentions légales</a></td>
			<tr>
			</table>
			</div>
			<div style="padding:20px">

   			<p><br>Depuis son apparition dans la littérature au 12e siècle, le Graal a engendré d'innombrables quêtes. <br>
La version présentée ici fut composée vers 1225, conservée à la Bibliothèque municipale de Lyon, est l’un des meilleurs manuscrits de ce célèbre roman du Moyen Age. </p>
<p>L’édition présentée ici est un <b>prototype</b>, et, nous l’espérons, un exemple de ce que peut désormais offrir la <b><i>‘philologie numérique’</i></b>, avec ses possibilités techniques nouvelles et son exigence de rigueur et de cohérence. C’est ainsi que le texte peut s’afficher sous un triple format – version courante, version diplomatique, version facsimilaire, la dernière étant la plus proche du texte tel que réalisé par le copiste médiéval, la première correspondant à une édition plus facile d’accès, semblable aux éditions habituelles des textes anciens. Vous pouvez aussi afficher les photographies du manuscrit et la traduction en français moderne. Ces différents éléments peuvent s'afficher côte à côte, en plusieurs colonnes.</p>
<p>Pour lire l'édition en ligne, cliquez sur le <a class="command" command="texts" path="/GRAAL">bouton 'Édition'</a> ci-dessus. </p>
<p>Fiche : <a class="command" command="record" path="/GRAAL" textid="qgraal_cm">FICHE</a></p>
<p>Vous pouvez également <a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm-index.pdf" target="_blank">télécharger au format PDF</a> une partie ou l'ensemble de cette édition.</p>
<p>Intégrée au portail de la Base de français médiéval, cette édition bénéficie de l'ensemble des fonctionnalités d'analyse textométrique proposées par la plateforme TXM. Faites un clic-droit sur le nom de corpus GRAAL dans le volet gauche de l'écran pour accéder à ces fonctionnalités.</p>
<p>Cette édition est accompagnée d'une <a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm_2013-07-intro.pdf" target="_blank">Introduction</a>, d'un <a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm_2013-07-inp.pdf" target="_blank">Index des noms propres</a> et d'un <a href="http://txm.ish-lyon.cnrs.fr/bfm/pdf/qgraal_cm_2013-07-glossaire.pdf" target="_blank">Glossaire</a> que vous pouvez visionner et télécharger au format PDF.</p>

<br>
	</div>
   		</div>
   		

</body>
</html>